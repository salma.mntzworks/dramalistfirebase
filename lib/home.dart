import 'package:flutter/material.dart';
import 'package:list_film/bookmarks.dart';
import 'package:list_film/movie_list.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        onTap: (int index) {
          switch (index) {
            case 0: const MovieList();
            break;
            case 1: const BookmarksPage();
          }
          setState(
                () {
              _selectedIndex = index;
            },
          );
        },
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home_filled),
            label: 'Home',
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.bookmark),
              label: 'Bookmark'
          )
        ],
      ),
      body: const SafeArea(
        child: MovieList()
      )
    );
  }
}
