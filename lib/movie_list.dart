import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';

class MovieList extends StatefulWidget {
  const MovieList({Key? key}) : super(key: key);

  @override
  State<MovieList> createState() => _MovieListState();
}

class _MovieListState extends State<MovieList> {
  final DatabaseReference _moviesRef = FirebaseDatabase.instance.ref().child('data');

  @override
  void initState() {
    _moviesRef.once().then((event) {
      var eventData = event.snapshot;
      var title = eventData.child('title').value.toString();
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FirebaseAnimatedList(
        query: _moviesRef,
        itemBuilder: (context, snapshot, animation, index) {
          return Container(
              height: 100,
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
              decoration: BoxDecoration(border: Border.all(color: Colors.grey, width: 1), borderRadius: BorderRadius.circular(8)),
              width: MediaQuery.of(context).size.width,
              child: Row(
                children: [
                  Image.network(snapshot.child('img').value.toString(), width: 62),
                  const SizedBox(width: 5),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                          width: 250,
                          child: Text(snapshot.child('title').value.toString(), style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w600, overflow: TextOverflow.clip),)),
                      const SizedBox(height: 4),
                      Text(snapshot.child('genre').value.toString()),
                    ],
                  ),
                ],
              )
          );
        },);
  }
}
